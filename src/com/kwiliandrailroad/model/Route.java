package com.kwiliandrailroad.model;

/**
 * Class that represents a one-way Route between two cities (origin and
 * destination)
 * 
 * @author Elton Soares
 *
 */
public class Route {
	private City origin;
	private City destination;
	private Integer distance;

	public Route() {
	}

	public Route(City origin, City destination, Integer distance) {
		super();
		setOrigin(origin);
		setDestination(destination);
		setDistance(distance);
	}

	public City getOrigin() {
		return origin;
	}

	public void setOrigin(City origin) {
		if (origin != null)
			if (!origin.equals(destination))
				this.origin = origin;
			else
				throw new IllegalArgumentException("Route origin has to be different than destination!");

		else
			throw new IllegalArgumentException("Route origin cannot be null!");
	}

	public City getDestination() {
		return destination;
	}

	public void setDestination(City destination) {
		if (destination != null)
			if (!destination.equals(origin))
				this.destination = destination;
			else
				throw new IllegalArgumentException("Route destination has to be different than origin!");
		else
			throw new IllegalArgumentException("Route destination cannot be null!");
	}

	public Integer getDistance() {
		return distance;
	}

	public void setDistance(Integer distance) {
		if (distance != null) {
			if (distance > 0)
				this.distance = distance;
			else
				throw new IllegalArgumentException("Route distance has to be higher than 0!");
		} else
			throw new IllegalArgumentException("Route distance cannot be null!");

	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Route) {
			Route route = (Route) obj;
			return origin.equals(route.getOrigin()) && destination.equals(route.getDestination())
					&& distance.equals(route.getDistance());
		} else {
			return false;
		}
	}

}
