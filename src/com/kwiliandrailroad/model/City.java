package com.kwiliandrailroad.model;

/**
 * Class that represents a city
 * 
 * @author Elton Soares
 *
 */
public class City {

	private String name;

	public City() {
	}

	public City(String name) {
		super();
		setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (name != null)
			this.name = name;
		else
			throw new IllegalArgumentException("City name cannot be null!");
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof City) {
			City city = (City) obj;
			return name.equals(city.getName());
		} else {
			return false;
		}
	}

}
