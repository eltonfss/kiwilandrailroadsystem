package com.kiwilandrailroad.view;

public class Main {
	public static void main(String[] args) {

		ConsoleInterface consoleInterface = new ConsoleInterface();
		consoleInterface.runKiwilandRailRoadSystem();
	}
}
