package com.kiwilandrailroad.view;

import java.util.Scanner;

import com.kiwilandrailroad.controller.RailroadManager;
import com.kiwilandrailroad.controller.RailroadManagerFactoryFromFile;
import com.kiwilandrailroad.controller.exceptions.NoSuchRouteException;

public class ConsoleInterface {

	private RailroadManager railroadManager = new RailroadManager();
	private Scanner scanner = new Scanner(System.in);

	public void runKiwilandRailRoadSystem() {
		System.out.println("Welcome to the Kiwiland Railroad System!");
		System.out.println("Please inform the route input file path:");

		String inputFilePath = scanner.nextLine();

		RailroadManagerFactoryFromFile railroadManagerFactory = new RailroadManagerFactoryFromFile(inputFilePath);
		railroadManager = railroadManagerFactory.create();

		System.out.println("Route insertion completed successfully!");

		showAndProcessMenuOptions();

		System.out.println("Thanks for using the Kiwiland Railroad System!");
		scanner.close();
	}

	private void showAndProcessMenuOptions() {
		boolean showMenuOptions = true;
		while (showMenuOptions) {
			printMenuOptions();
			Integer option = scanner.nextInt();
			String originCityName = "";
			String destinationCityName = "";
			switch (option) {
			case 1:
				String[] cityNames = readCityNames();
				calculateAndPrintRouteDistance(cityNames);
				break;
			case 2:
				originCityName = readOriginCityName();
				destinationCityName = readDestinationCityName();
				Integer minimumStops = readMinimumOfStops();
				Integer maximumStops = readMaximumOfStops();
				calculateAndPrintNumberOfTrips(originCityName, destinationCityName, minimumStops, maximumStops);
				break;
			case 3:
				originCityName = readOriginCityName();
				destinationCityName = readDestinationCityName();
				calculateAndPrintLengthOfShortestRoute(originCityName, destinationCityName);
				break;
			case 4:
				originCityName = readOriginCityName();
				destinationCityName = readDestinationCityName();
				Integer maximumDistance = readMaximumDistance();
				calculateAndPrintNumberOfDifferentRoutes(originCityName, destinationCityName, maximumDistance);
				break;
			case 0:
				showMenuOptions = false;
				continue;
			default:
				System.err.println("Invalid menu option!");
				continue;
			}

			scanner.nextLine();
			System.out.println("Do you want to make another opperation (Y/N) ?");
			showMenuOptions = scanner.nextLine().equals("Y");
		}
	}

	private void calculateAndPrintNumberOfDifferentRoutes(String originCityName, String destinationCityName,
			Integer maximumDistance) {
		System.out.println("The number of different routes is "
				+ railroadManager.getNumberOfDifferentRoutes(originCityName, destinationCityName, maximumDistance));
	}

	private Integer readMaximumDistance() {
		System.out.println("Insert the maximum distance of the route: ");
		Integer maximumDistance = scanner.nextInt();
		return maximumDistance;
	}

	private void calculateAndPrintLengthOfShortestRoute(String originCityName, String destinationCityName) {
		System.out.println("The length of the shortest route is "
				+ railroadManager.getLengthOfShortestRoute(originCityName, destinationCityName));
	}

	private void calculateAndPrintNumberOfTrips(String originCityName, String destinationCityName, Integer minimumStops,
			Integer maximumStops) {
		System.out.println("The number trips is "
				+ railroadManager.getNumberOfTrips(originCityName, destinationCityName, minimumStops, maximumStops));
	}

	private Integer readMaximumOfStops() {
		System.out.println("Insert the maximum number of stops: ");
		Integer maximumStops = scanner.nextInt();
		return maximumStops;
	}

	private Integer readMinimumOfStops() {
		System.out.println("Insert the minimum number of stops: ");
		Integer minimumStops = scanner.nextInt();
		return minimumStops;
	}

	private String readDestinationCityName() {
		String destinationCityName;
		System.out.println("Insert the name of the destination city: ");
		destinationCityName = scanner.nextLine();
		return destinationCityName;
	}

	private String readOriginCityName() {
		String originCityName;
		System.out.println("Insert the name of the origin city: ");
		scanner.nextLine();
		originCityName = scanner.nextLine();
		return originCityName;
	}

	private void calculateAndPrintRouteDistance(String[] cityNames) {
		try {
			System.out.println("The route distance is " + railroadManager.getDistance(cityNames));
		} catch (NoSuchRouteException e) {
			System.err.println("NO SUCH ROUTE");
		}
	}

	private String[] readCityNames() {
		System.out.println("Insert the route in the format <CityName>-<CityName>-...-<CityName>:");
		scanner.nextLine();
		String[] cityNames = scanner.nextLine().split("-");
		return cityNames;
	}

	private void printMenuOptions() {
		System.out.println("Please select one of the menu options: ");
		System.out.println("1 - Calculate distance of a route");
		System.out.println(
				"2 - Calculate the number of possible trips between two cities with a given minimum of stops and a given maximum of stops");
		System.out.println("3 - Calculate the lenght of the shortest route between two cities");
		System.out.println(
				"4 - Calculate the number different possible routes between two cities with a given maximum distance");
		System.out.println("0 - Exit KiwilandRailroadSystem");
	}

}
