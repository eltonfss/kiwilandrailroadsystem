package com.kiwilandrailroad.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import com.kiwilandrailroad.controller.exceptions.DuplicateRouteException;

public class RailroadManagerFactoryFromFile implements RailrodaManagerFactory {

	private RailroadManager railroadManager;
	private String inputFilePath;

	public RailroadManagerFactoryFromFile(String filePath) {
		super();
		this.railroadManager = new RailroadManager();
		this.inputFilePath = filePath;
	}

	private RailroadManager getRailroadManager() {
		return railroadManager;
	}

	private RailroadManagerFactoryFromFile addRoutesFromFile(String filePath) {
		String fileContent = getFileContent(filePath);
		addRoutes(fileContent);
		return this;
	}

	private void addRoutes(String fileContent) {
		String[] fileContentArray = fileContent.split(" ");

		for (int i = 1; i < fileContentArray.length; i++) {
			String originCityName = String.valueOf(fileContentArray[i].charAt(0));
			String destinationCityName = String.valueOf(fileContentArray[i].charAt(1));
			Integer distance = Character.getNumericValue(fileContentArray[i].charAt(2));
			try {
				railroadManager.addRoute(originCityName, destinationCityName, distance);
			} catch (DuplicateRouteException e) {
				System.err.println(e.getMessage()
						+ " Please, check your input file for next insertions. \n The system will keep inserting the other routes on the given file.");
			}
		}
	}

	private String getFileContent(String inputFilePath) {
		List<String> lines = new ArrayList<>();

		try {
			lines = Files.readAllLines(Paths.get(inputFilePath));
		} catch (IOException e) {
			e.printStackTrace();
		}

		StringBuilder builder = new StringBuilder();
		for (String line : lines) {
			builder.append(line);
		}

		String fileContent = builder.toString();
		return fileContent;
	}

	@Override
	public RailroadManager create() {
		return addRoutesFromFile(inputFilePath).getRailroadManager();
	}
}
