package com.kiwilandrailroad.controller;

public interface RailrodaManagerFactory {
	RailroadManager create();
}
