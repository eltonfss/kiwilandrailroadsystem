package com.kiwilandrailroad.controller;

import java.util.ArrayList;
import java.util.List;

import com.kiwilandrailroad.controller.exceptions.DuplicateRouteException;
import com.kiwilandrailroad.controller.exceptions.NoSuchRouteException;
import com.kwiliandrailroad.model.City;
import com.kwiliandrailroad.model.Route;

/**
 * Provides a control interface for the view to interact with the Railroad
 * 
 * @author Elton Soares
 *
 */
public class RailroadManager {

	private List<Route> routes;

	public RailroadManager() {
		routes = new ArrayList<>();
	}
	
	public List<Route> getRoutes() {
		return routes;
	}

	/**
	 * Add a route to the Railroad with given origin, destination and distance.
	 * 
	 * @param originCityName
	 * @param destinationCityName
	 * @param distance
	 * @throws DuplicateRouteException 
	 */
	public void addRoute(String originCityName, String destinationCityName, Integer distance) throws DuplicateRouteException {

		City origin = new City(originCityName);
		City destination = new City(destinationCityName);
		Route route = new Route(origin, destination, distance);

		if (!exists(route)) {
			routes.add(route);
		} else {
			throw new DuplicateRouteException();
		}
	}

	/**
	 * Calculate the sum of the distance of the routes between the cities passed
	 * as parameter. Throws NoSuchRouteException if any of the routes does not
	 * exist.
	 * 
	 * @param cityNames
	 * @return total distance as Integer
	 * @throws NoSuchRouteException
	 */
	public Integer getDistance(String... cityNames) throws NoSuchRouteException {

		Integer totalDistance = 0;

		List<City> cities = getCities(cityNames);

		for (int i = 0; i < cities.size() - 1; i++) {
			City origin = cities.get(i);
			City destination = cities.get(i + 1);
			totalDistance += getDistance(origin, destination);
		}

		return totalDistance;
	}

	/**
	 * Find the route between two cities (if exists) and return it's distance.
	 * Throws NoSuchRouteException if none of the routes has the given origin
	 * and destination cities.
	 * 
	 * @param origin
	 * @param destination
	 * @return distance of the route as Integer
	 * @throws NoSuchRouteException
	 */
	private Integer getDistance(City origin, City destination) throws NoSuchRouteException {

		Integer distance = 0;

		for (Route route : routes) {
			if (route.getOrigin().equals(origin) && route.getDestination().equals(destination)) {
				distance = route.getDistance();
			}
		}

		if (distance == 0) {
			throw new NoSuchRouteException(origin, destination);
		}

		return distance;
	}

	/**
	 * Converts a String array of City names into a List of City objects
	 * 
	 * @param cityNames
	 * @return an ArrayList of City objects
	 */
	private List<City> getCities(String... cityNames) {
		List<City> cities = new ArrayList<>();
		if (cityNames != null) {
			if (cityNames.length > 1) {
				for (String cityName : cityNames) {
					cities.add(new City(cityName));
				}
			} else
				throw new IllegalArgumentException(
						"Insert at least two city names to calculate the distance of the route!");
		}
		return cities;
	}

	/**
	 * Checks if a given route has already been added to the Railroad
	 * 
	 * @param route
	 * @return true if route is on the list, false if if isn't.
	 */
	private boolean exists(Route route) {
		for (Route r : routes) {
			if (r.equals(route)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Calculates the number of possible trips from the given origin to the
	 * given destiny with the number stops higher or equal to the given minimum
	 * and lower or equal to the given maximum
	 * 
	 * @param originCityName
	 * @param destinationCityName
	 * @param minimumStops
	 * @param maximumStops
	 * @return the number of possible trips as Integer
	 */
	public Integer getNumberOfTrips(String originCityName, String destinationCityName, Integer minimumStops,
			Integer maximumStops) {

		City origin = new City(originCityName);
		City destination = new City(destinationCityName);
		Integer numberOfTrips = 0;

		for (Route route : routes) {
			if (route.getOrigin().equals(origin)) {
				numberOfTrips += getNumberOfTrips(destination, route, minimumStops, maximumStops, 1);
			}
		}

		return numberOfTrips;

	}

	/**
	 * Calculates the number of possible trips from the destination of the given
	 * route to the given destination city with totalStops higher or equal to
	 * the given minimum and lower or equal to the given maximum in a recursive
	 * way.
	 * 
	 * @param destination
	 * @param route
	 * @param minimumStops
	 * @param maximumStops
	 * @param totalStops
	 * @return the number of possible trips as Integer
	 */
	private Integer getNumberOfTrips(City destination, Route route, Integer minimumStops, Integer maximumStops,
			Integer totalStops) {
		Integer numberOfTrips = 0;
		if (route.getDestination().equals(destination) && totalStops >= minimumStops && totalStops <= maximumStops) {
			numberOfTrips++;
		}
		for (Route nextRoute : routes) {
			if (nextRoute.getOrigin().equals(route.getDestination()) && totalStops <= maximumStops) {
				numberOfTrips += getNumberOfTrips(destination, nextRoute, minimumStops, maximumStops, totalStops + 1);
			}
		}
		return numberOfTrips;
	}

	/**
	 * Calculates the length of the shortest route between the given origin and
	 * destiny
	 * 
	 * @param originCityName
	 * @param destinationCityName
	 * @return the total length(distance) of the shortest route as Integer
	 */
	public Integer getLengthOfShortestRoute(String originCityName, String destinationCityName) {

		City origin = new City(originCityName);
		City destination = new City(destinationCityName);
		Integer lengthOfShortestRoute = Integer.MAX_VALUE;

		for (Route route : routes) {
			if (route.getOrigin().equals(origin)) {
				Integer lengthOfShortestRouteStartingAtCurrentRoute = getLengthOfShortestRoute(destination, route);
				if (lengthOfShortestRoute > lengthOfShortestRouteStartingAtCurrentRoute)
					lengthOfShortestRoute = lengthOfShortestRouteStartingAtCurrentRoute;
			}

		}

		return lengthOfShortestRoute;
	}

	/**
	 * Calculates the length of the shortest route between the destination of
	 * the given route and the given destination city in a recursive way.
	 * 
	 * @param destination
	 * @param route
	 * @return the total length(distance) of the shortest route as Integer
	 */
	private Integer getLengthOfShortestRoute(City destination, Route route) {
		Integer lengthOfShortestRoute = Integer.MAX_VALUE;
		if (route.getDestination().equals(destination))
			lengthOfShortestRoute = route.getDistance();
		else
			for (Route nextRoute : routes) {
				if (nextRoute.getOrigin().equals(route.getDestination()) && !isCycle(route, nextRoute)) {
					Integer lengthOfShortestsRouteStartingAtCurrentRoute = route.getDistance()
							+ getLengthOfShortestRoute(destination, nextRoute);
					if (lengthOfShortestRoute > lengthOfShortestsRouteStartingAtCurrentRoute)
						lengthOfShortestRoute = lengthOfShortestsRouteStartingAtCurrentRoute;
				}
			}
		return lengthOfShortestRoute;

	}

	/**
	 * Checks if two routes form a cycle (Destination of a = Origin of b,
	 * Destination of b = Origin of a)
	 * 
	 * @param a
	 * @param b
	 * @return true if there is a cycle, false if there isn't
	 */
	private boolean isCycle(Route a, Route b) {
		return b.getDestination().equals(a.getOrigin()) && a.getDestination().equals(b.getOrigin());
	}

	/**
	 * Calculates the number of different routes from the given origin to the
	 * given destination with total distance smaller than the given maximum
	 * 
	 * @param originCityName
	 * @param destinationCityName
	 * @param maximumDistance
	 * @return the number of different routes as Integer
	 */
	public Integer getNumberOfDifferentRoutes(String originCityName, String destinationCityName,
			Integer maximumDistance) {
		Integer numberOfDifferentRoutes = 0;
		City origin = new City(originCityName);
		City destination = new City(destinationCityName);
		for (Route route : routes) {
			if (route.getOrigin().equals(origin)) {
				numberOfDifferentRoutes += getNumberOfDifferentRoutes(destination, route,
						maximumDistance - route.getDistance());
			}
		}
		return numberOfDifferentRoutes;
	}

	/**
	 * Calculates the number of different routes from the destination of the
	 * given route to the given destination city with total distance smaller
	 * than the given maximum in a recursive way.
	 * 
	 * @param destination
	 * @param route
	 * @param maximumDistance
	 * @return the number of different routes as Integer
	 */
	private Integer getNumberOfDifferentRoutes(City destination, Route route, Integer maximumDistance) {
		Integer numberOfDifferentRoutes = 0;

		if (route.getDestination().equals(destination) && maximumDistance > 0) {
			numberOfDifferentRoutes++;
		}

		for (Route nextRoute : routes) {
			if (nextRoute.getOrigin().equals(route.getDestination()) && maximumDistance > 0) {
				numberOfDifferentRoutes += getNumberOfDifferentRoutes(destination, nextRoute,
						maximumDistance - nextRoute.getDistance());
			}
		}

		return numberOfDifferentRoutes;
	}

}
