package com.kiwilandrailroad.controller.exceptions;

import com.kwiliandrailroad.model.City;

public class NoSuchRouteException extends Exception {

	private static final long serialVersionUID = 1L;
	protected City origin;
	protected City destination;

	public NoSuchRouteException(City origin, City destination) {
		this.origin = origin;
		this.destination = destination;
	}

	@Override
	public String getMessage() {
		return "There is no route from city " + origin.getName() + " to city " + destination.getName() + "!";
	}

}
