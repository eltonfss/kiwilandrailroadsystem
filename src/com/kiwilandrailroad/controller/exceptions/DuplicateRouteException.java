package com.kiwilandrailroad.controller.exceptions;

public class DuplicateRouteException extends Exception {

	private static final long serialVersionUID = 1L;

	@Override
	public String getMessage() {
		return "Duplicate route insertion attempt!";
	}

}
