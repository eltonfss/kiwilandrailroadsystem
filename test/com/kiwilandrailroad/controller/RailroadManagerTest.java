package com.kiwilandrailroad.controller;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.kiwilandrailroad.controller.RailroadManager;
import com.kiwilandrailroad.controller.exceptions.DuplicateRouteException;
import com.kiwilandrailroad.controller.exceptions.NoSuchRouteException;

public class RailroadManagerTest {

	private static RailroadManager railroadManager;

	@BeforeClass
	public static void initialize() throws DuplicateRouteException {
		railroadManager = new RailroadManager();
		railroadManager.addRoute("A", "B", 5);
		railroadManager.addRoute("B", "C", 4);
		railroadManager.addRoute("C", "D", 8);
		railroadManager.addRoute("D", "C", 8);
		railroadManager.addRoute("D", "E", 6);
		railroadManager.addRoute("A", "D", 5);
		railroadManager.addRoute("C", "E", 2);
		railroadManager.addRoute("E", "B", 3);
		railroadManager.addRoute("A", "E", 7);

	}

	@Test
	public void getDistancePositiveTest() throws NoSuchRouteException {
		Assert.assertEquals(9, railroadManager.getDistance("A", "B", "C").intValue());
		Assert.assertEquals(5, railroadManager.getDistance("A", "D").intValue());
		Assert.assertEquals(13, railroadManager.getDistance("A", "D", "C").intValue());
		Assert.assertEquals(22, railroadManager.getDistance("A", "E", "B", "C", "D").intValue());
	}

	@Test
	public void getNumberOfTrips() {
		Assert.assertEquals(2, railroadManager.getNumberOfTrips("C", "C", 0, 3).intValue());
		Assert.assertEquals(3, railroadManager.getNumberOfTrips("A", "C", 4, 4).intValue());
	}

	@Test
	public void getLengthOfShortestRouteTest() {
		Assert.assertEquals(9, railroadManager.getLengthOfShortestRoute("A", "C").intValue());
		Assert.assertEquals(9, railroadManager.getLengthOfShortestRoute("B", "B").intValue());
	}

	@Test
	public void getNumberOfDifferentRoutesByDistanceTest() {
		Assert.assertEquals(7, railroadManager.getNumberOfDifferentRoutes("C", "C", 30).intValue());
	}

	@Test(expected = NoSuchRouteException.class)
	public void getDistanceNegativeTest() throws NoSuchRouteException {
		railroadManager.getDistance("A", "E", "D");
	}

}
