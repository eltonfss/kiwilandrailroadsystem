package com.kiwilandrailroad.controller;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.kiwilandrailroad.controller.RailroadManager;
import com.kiwilandrailroad.controller.RailroadManagerFactoryFromFile;
import com.kwiliandrailroad.model.Route;

public class RailroadManagerFactoryTest {

	private String filepath = "test/com/kiwilandrailroad/controller/RailroadInput.txt";

	@Test
	public void createRailroadManagerFromFileTest() {
		RailroadManagerFactoryFromFile railRoadManagerFactory = new RailroadManagerFactoryFromFile(filepath);
		RailroadManager railroadManager = railRoadManagerFactory.create();
		List<Route> routes = railroadManager.getRoutes();
		Assert.assertEquals("A", routes.get(0).getOrigin().getName());
		Assert.assertEquals("B", routes.get(0).getDestination().getName());
		Assert.assertEquals(5, routes.get(0).getDistance().intValue());
		Assert.assertEquals("B", routes.get(1).getOrigin().getName());
		Assert.assertEquals("C", routes.get(1).getDestination().getName());
		Assert.assertEquals(4, routes.get(1).getDistance().intValue());
		Assert.assertEquals("C", routes.get(2).getOrigin().getName());
		Assert.assertEquals("D", routes.get(2).getDestination().getName());
		Assert.assertEquals(8, routes.get(2).getDistance().intValue());
		Assert.assertEquals("D", routes.get(3).getOrigin().getName());
		Assert.assertEquals("C", routes.get(3).getDestination().getName());
		Assert.assertEquals(8, routes.get(3).getDistance().intValue());
		Assert.assertEquals("D", routes.get(4).getOrigin().getName());
		Assert.assertEquals("E", routes.get(4).getDestination().getName());
		Assert.assertEquals(6, routes.get(4).getDistance().intValue());
		Assert.assertEquals("A", routes.get(5).getOrigin().getName());
		Assert.assertEquals("D", routes.get(5).getDestination().getName());
		Assert.assertEquals(5, routes.get(5).getDistance().intValue());
		Assert.assertEquals("C", routes.get(6).getOrigin().getName());
		Assert.assertEquals("E", routes.get(6).getDestination().getName());
		Assert.assertEquals(2, routes.get(6).getDistance().intValue());
		Assert.assertEquals("E", routes.get(7).getOrigin().getName());
		Assert.assertEquals("B", routes.get(7).getDestination().getName());
		Assert.assertEquals(3, routes.get(7).getDistance().intValue());
		Assert.assertEquals("A", routes.get(8).getOrigin().getName());
		Assert.assertEquals("E", routes.get(8).getDestination().getName());
		Assert.assertEquals(7, routes.get(8).getDistance().intValue());
	}

}
